import { ChakraProvider, theme } from '@chakra-ui/react';
import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Detail from './pages/Detail';
import Home from './pages/Home';
import { Provider } from 'react-redux';
import store from './redux';

export default function App() {
  return (
    <Provider store={store}>
      <ChakraProvider theme={theme}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="detail/:id" element={<Detail />} />
        </Routes>
      </ChakraProvider>
    </Provider>
  );
}
