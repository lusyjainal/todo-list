import Empty from './activity-empty-state.png';
import TodoEmpty from './todo-empty-state.png';
import RemoveIcon from './remove-icon.png';
import PencilIcon from './pencil-icon.png';
import ArrowLeft from './arrow-left.png';
import AlertDelete from './alert-delete.png';
import Filter from './filter.png';
import Terbaru from './terbaru.png';
import Terlama from './terlama.png';
import Az from './a-z.png';
import Za from './z-a.png';
import BelumSelesai from './belum-selesai.png';
import Remove from './remove.png';

const ImgAssets = {
    Empty,
    TodoEmpty,
    RemoveIcon,
    PencilIcon,
    ArrowLeft,
    AlertDelete,
    Filter,
    Terbaru,
    Terlama,
    Az,
    Za,
    BelumSelesai,
    Remove
}

export default ImgAssets;