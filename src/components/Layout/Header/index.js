import { Container, Heading, VStack } from '@chakra-ui/react';

export default function Header() {
  return (
    <VStack bg="#16ABF8" pt={38} pb={38}>
      <Container maxW="1000px" color="white">
        <Heading as="h1" fontSize={24} fontWeight="bold" data-cy="header-title">
          TO DO LIST APP
        </Heading>
      </Container>
    </VStack>
  );
}
