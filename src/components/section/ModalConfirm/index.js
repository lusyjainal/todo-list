import React from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Text,
  Image,
} from '@chakra-ui/react';
import ImgAssets from '../../../assets/images';

export default function index({ isOpen, setIsOpen, desc, title, handleClickDelete }) {
  const onClose = () => setIsOpen(!isOpen);

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay data-cy="modal-delete" onClick={() => setIsOpen(!isOpen)} />
      <ModalContent pt={30} pb={30} data-cy="modal-delete">
        <ModalHeader display="flex" justifyContent="center">
          <Image
            src={ImgAssets.AlertDelete}
            alt="delete confirm"
            width="63px"
            height="56px"
          />
        </ModalHeader>
        <ModalBody
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          alignContent="center"
        >
          <Text>{desc}</Text>
          <Text fontWeight="bold">“{title}”?</Text>
        </ModalBody>

        <ModalFooter display="flex" justifyContent="center">
          <Button
            data-cy="modal-delete-cancel-button"
            borderRadius={50}
            colorScheme="gray"
            mr={3}
            onClick={onClose}
          >
            Batal
          </Button>
          <Button
            data-cy="modal-delete-confirm-button"
            borderRadius={50}
            colorScheme="red"
            mr={3}
            onClick={() => handleClickDelete()}
          >
            Hapus
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
