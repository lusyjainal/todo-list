import { CheckIcon, ChevronDownIcon } from '@chakra-ui/icons';
import {
  Button,
  FormControl,
  FormLabel,
  Grid,
  Input,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  FormHelperText,
} from '@chakra-ui/react';
import styled from '@emotion/styled';
import axios from 'axios';
import { Formik } from 'formik';
import { useState } from 'react';
import { dataOption } from './__mocks__';
import { object, string } from 'yup';

const Bullet = styled.div`
  height: 10px;
  width: 10px;
  margin: 0 10px;
  border-radius: 50%;
  background-color: ${({ bgColor }) => (bgColor ? bgColor : '#FFCE31')};
`;

export default function ModalFormContainer({
  todo,
  mainActions: { getTodo },
  idActivity,
  id,
  isOpen,
  setIsOpen,
  getData,
}) {
  const [title, setTitle] = useState('');
  const [priority, setPriority] = useState('');
  const [isLoading, setLoading] = useState(false);
  const onClose = () => setIsOpen(!isOpen);

  const handleAddEditTodo = (values, { setSubmitting }) => {
    setLoading(true);
    if (values.id) {
      axios
        .patch(`https://todo.api.devcode.gethired.id/todo-items/${values.id}`, {
          activity_group_id: idActivity,
          title: values.title,
          priority: values.priority,
        })
        .then(() => {
          getData();
          setIsOpen(false);
          setSubmitting(false);
        })
        .catch(err => console.log(err));
      setLoading(false);
    } else {
      axios
        .post(`https://todo.api.devcode.gethired.id/todo-items`, {
          activity_group_id: idActivity,
          title: values.title,
          priority: values.priority,
        })
        .then(() => {
          getData();
          setIsOpen(false);
          setSubmitting(false);
        })
        .catch(err => console.log(err));
      setLoading(false);
    }
  };

  const handleReset = () => {
    setTitle('');
    setPriority('');
    setIsOpen(false);
    getTodo({});
  };

  return (
    <Modal data-cy="modal-add" isOpen={isOpen} onClose={onClose} size="xl">
      <ModalOverlay />
      <ModalContent data-cy="modal-add">
        <Formik
          initialValues={{
            id: todo.id || '',
            title: todo.title || '',
            priority: todo.priority || '',
          }}
          validationSchema={object({
            title: string().required(),
            priority: string().required(),
          })}
          onSubmit={handleAddEditTodo}
        >
          {({
            values,
            errors,
            touched,
            setFieldValue,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            dirty,
            isValid,
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <ModalHeader
                data-cy="modal-add-title"
                fontSize={18}
                fontWeight={600}
                color="#111111"
              >
                {values.id ? 'Edit Item' : 'Tambah List Item'}
              </ModalHeader>
              <ModalCloseButton
                onClick={() => handleReset()}
                data-cy="modal-add-close-button"
              />
              <ModalBody>
                <FormControl isInvalid={errors.title && touched.title}>
                  <FormLabel
                    data-cy={
                      todo.id ? 'modal-edit-name-title' : 'modal-add-name-title'
                    }
                    fontSize={12}
                    color="#111111"
                    fontWeight={600}
                  >
                    NAMA LIST ITEM
                  </FormLabel>
                  <Input
                    type="text"
                    placeholder="Tambahkan nama list item"
                    data-cy="modal-add-name-input"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.title}
                    name="title"
                    // onChange={e => setTitle(e.target.value)}
                  />
                  {errors.title && touched.title ? (
                    <FormHelperText color="red">{errors.title}</FormHelperText>
                  ) : null}
                </FormControl>
                <FormControl mt={26} isInvalid={errors.priority}>
                  <FormLabel
                    data-cy="modal-add-priority-title"
                    fontSize={12}
                    color="#111111"
                    fontWeight={600}
                  >
                    PRIORITY
                  </FormLabel>
                  <Menu>
                    <MenuButton
                      data-cy="modal-add-priority-dropdown"
                      width={205}
                      fontSize={16}
                      fontWeight={400}
                      variant="outline"
                      as={Button}
                      rightIcon={
                        values.priority === '' ? (
                          <ChevronDownIcon />
                        ) : (
                          <CheckIcon />
                        )
                      }
                    >
                      {values.priority === ''
                        ? 'Pilih priority'
                        : dataOption.map(list => {
                            if (values.priority === list.priority) {
                              return (
                                <Grid display="flex" alignItems="center">
                                  <Bullet bgColor={list.color} />
                                  <Text>{list.title}</Text>
                                </Grid>
                              );
                            }
                          })}
                    </MenuButton>
                    <MenuList>
                      {dataOption.map(list => (
                        <MenuItem
                          data-cy="modal-add-priority-item"
                          display="flex"
                          justifyContent="space-between"
                          alignItems="center"
                          onClick={() =>
                            setFieldValue('priority', list.priority)
                          }
                        >
                          <Grid display="flex" alignItems="center">
                            <Bullet bgColor={list.color} />
                            <Text>{list.title}</Text>
                          </Grid>
                          {list.priority === values.priority && <CheckIcon />}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </Menu>
                  {errors.priority ? (
                    <FormHelperText color="red">
                      {errors.priority}
                    </FormHelperText>
                  ) : null}
                </FormControl>
              </ModalBody>

              <ModalFooter>
                <Button
                  id="AddFormSubmit"
                  data-cy="modal-add-save-button"
                  variant="solid"
                  bg="#16ABF8"
                  mr={3}
                  borderRadius="50px"
                  colorScheme="teal"
                  type="submit"
                  disabled={isSubmitting || !isValid || !dirty}
                >
                  Simpan
                </Button>
              </ModalFooter>
            </form>
          )}
        </Formik>
      </ModalContent>
    </Modal>
  );
}
