import { connect } from 'react-redux';
import ModalFormContainer from './ModalFormContainer';
import { bindActionCreators } from 'redux';
import * as mainActions from '../../../redux/todo/action';

const mapStateToProps = state => {
  return {
    todo: state.todo,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    mainActions: bindActionCreators(mainActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalFormContainer);
