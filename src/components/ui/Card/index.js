import { DeleteIcon } from '@chakra-ui/icons';
import { Box, Grid, Heading, Text } from '@chakra-ui/react';
import { NavLink } from 'react-router-dom';
import moment from 'moment';

moment.locale('id');

const Card = ({ id, title, date, handleDelete }) => {
  return (
    <Box
      id={id}
      data-cy="activity-item"
      p={5}
      shadow="md"
      borderWidth="1px"
      bg="white"
      borderRadius={12}
      height={234}
      width="100%"
      display="flex"
      flexDirection="column"
      justifyContent="space-between"
    >
      <NavLink to={`/detail/${id}`} data-cy="activity-item">
        <Heading
          cursor="pointer"
          height="160px"
          fontSize="18px"
          data-cy="activity-item-title"
        >
          {title}
        </Heading>
      </NavLink>
      <Grid display="flex" justifyContent="space-between">
        <Text
          fontSize={14}
          color="#888888"
          fontWeight={500}
          data-cy="activity-item-date"
        >
          {moment(date).format('DD MMMM YY')}
        </Text>
        <DeleteIcon
          color="#888888"
          cursor="pointer"
          data-cy="activity-item-delete-button"
          onClick={() => handleDelete(title, id)}
        />
      </Grid>
    </Box>
  );
};

export default Card;
