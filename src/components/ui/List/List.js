import { DeleteIcon } from '@chakra-ui/icons';
import { Box, Grid, Image, Text } from '@chakra-ui/react';
import styled from '@emotion/styled';
import { useState } from 'react';
import ImgAssets from '../../../assets/images';
import './style.css';

const Bullet = styled.div`
  height: 10px;
  width: 10px;
  margin: 0 16px;
  border-radius: 50%;
  background-color: ${({ bgColor }) => (bgColor ? bgColor : '#FFCE31')};
`;

const Checked = styled.input`
  background-color: #16abf8;
  border-color: #16abf8;
  width: 20px;
  height: 20px;
  cursor: pointer;
`;

const List = ({
  todo,
  handleDelete,
  handleEdit,
  handleChecked,
  mainActions: { getTodo },
}) => {
  const [isChecked, setIsChecked] = useState(false);
  const { id, priority, title, is_active } = todo;

  const handleOnChange = () => {
    setIsChecked(!isChecked);
    handleChecked(id, !isChecked);
  };

  const bgColor = bg => {
    switch (bg) {
      case 'very-high':
        return '#ED4C5C';
      case 'high':
        return '#F8A541';
      case 'normal':
        return '#00A790';
      case 'low':
        return '#428BC1';
      case 'very-low':
        return '#8942C1';
      default:
        return '#8942C1';
    }
  };

  return (
    <Box
      id={id}
      data-cy="todo-item"
      p={5}
      shadow="md"
      borderWidth="1px"
      bg="white"
      borderRadius={12}
      height="80px"
      width="100%"
      display="flex"
      alignItems="center"
      alignContent="center"
      justifyContent="space-between"
    >
      <Grid display="flex" justifyContent="center" alignItems="center">
        <Checked
          data-cy="todo-item-checkbox"
          type="checkbox"
          id="topping"
          name="topping"
          value="Paneer"
          checked={isChecked == is_active}
          onChange={handleOnChange}
        />
        <Bullet
          bgColor={bgColor(priority)}
          data-cy="todo-item-priority-indicator"
        />
        <Text
          as="h1"
          data-cy="todo-item-title"
          style={{
            textDecoration: isChecked == is_active && 'line-through',
          }}
          fontSize="18px"
          color={isChecked == is_active ? '#888888' : '#111111'}
          fontWeight={500}
        >
          {title}
        </Text>
        <Image
          src={ImgAssets.PencilIcon}
          alt="pencil-icon"
          ml={18}
          cursor="pointer"
          onClick={() => {
            handleEdit();
            getTodo(todo);
          }}
        />
      </Grid>
      <Image
        src={ImgAssets.RemoveIcon}
        alt="delete-icon"
        data-cy="todo-item-delete-button"
        cursor="pointer"
        onClick={() => {
          handleDelete();
          getTodo(todo);
        }}
      />
    </Box>
  );
};

export default List;
