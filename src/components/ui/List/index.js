import { connect } from 'react-redux';
import List from './List';
import { bindActionCreators } from 'redux';
import * as mainActions from '../../../redux/todo/action';

const mapDispatchToProps = dispatch => {
  return {
    mainActions: bindActionCreators(mainActions, dispatch)
  };
};

export default connect(null, mapDispatchToProps)(List);
