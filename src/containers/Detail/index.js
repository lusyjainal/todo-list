import {
  Box,
  Container,
  Grid,
  GridItem,
  Image,
  VStack,
  Heading,
} from '@chakra-ui/react';
import ImgAssets from '../../assets/images';
import { List } from '../../components/ui';

export default function index({
  data,
  handleDelete,
  handleEdit,
  handleChecked,
}) {
  return (
    <>
      {(() => {
        if (data.length > 0)
          return (
            <Container maxW="1000px">
              <Grid templateColumns="repeat(1, 1fr)" gap={6}>
                {data.map(list => (
                  <GridItem w="100%" key={list.id}>
                    <List
                      todo={list}
                      handleDelete={handleDelete}
                      handleEdit={handleEdit}
                      handleChecked={handleChecked}
                    />
                  </GridItem>
                ))}
              </Grid>
            </Container>
          );
        if (data.length === 0)
          return (
            <Box textAlign="center" fontSize="xl">
              <Grid p={3}>
                <VStack spacing={8}>
                  <Image
                    data-cy="todo-empty-state"
                    width={700}
                    objectFit="cover"
                    src={ImgAssets.TodoEmpty}
                    alt="empty"
                  />
                </VStack>
              </Grid>
            </Box>
          );
      })()}
    </>
  );
}
