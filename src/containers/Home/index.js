import {
  Box,
  Container,
  Grid,
  GridItem,
  Heading,
  Image,
  VStack,
} from '@chakra-ui/react';
import React from 'react';
import ImgAssets from '../../assets/images';
import { Card } from '../../components/ui';

export default function index({ data, handleDelete }) {
  return (
    <>
      {(() => {
        if (data.length > 0)
          return (
            <Container maxW="1000px">
              <Grid templateColumns="repeat(4, 1fr)" gap={6}>
                {data.map(item => (
                  <GridItem key={item.id} w="100%">
                    <Card
                      title={item.title}
                      id={item.id}
                      date={item.created_at}
                      handleDelete={handleDelete}
                    />
                  </GridItem>
                ))}
              </Grid>
            </Container>
          );
        if (data.length === 0)
          return (
            <Box textAlign="center" fontSize="xl">
              <Grid p={3}>
                <VStack spacing={8}>
                  <Image
                    data-cy="activity-empty-state"
                    width={700}
                    objectFit="cover"
                    src={ImgAssets.Empty}
                    alt="empty"
                  />
                </VStack>
              </Grid>
            </Box>
          );
      })()}
    </>
  );
}
