import React, { useState, useEffect } from 'react';
import { AddIcon } from '@chakra-ui/icons';
import {
  Button,
  Container,
  Grid,
  GridItem,
  Image,
  Input,
  Text,
  VStack,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  useToast,
  Box,
} from '@chakra-ui/react';
import { useNavigate, useParams } from 'react-router-dom';
import ImgAssets from '../../assets/images';
import Layout from '../../components/Layout';
import { ModalConfirm, ModalForm } from '../../components/section';
import { DetailContainer } from '../../containers';
import { dataOption } from './__mocks__';
import { CheckIcon } from '@chakra-ui/icons';
import axios from 'axios';

function Detail({ todo }) {
  const [data, setData] = useState([]);
  const [activity, setActivity] = useState('...');
  const [isInput, setInput] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenDelete, setIsOpenDelete] = useState(false);
  const [priority, setPriority] = useState('terbaru');
  const [isLoading, setLoading] = useState(false);
  const toast = useToast();

  const navigate = useNavigate();
  const { id } = useParams();

  const getData = () => {
    setLoading(true);
    axios
      .get(`https://todo.api.devcode.gethired.id/activity-groups/${id}`)
      .then(response => {
        const { todo_items, title } = response.data;
        setData(todo_items);
        setActivity(title);
        setLoading(false);
        setPriority('terbaru');
      })
      .catch(err => console.log(err));
  };

  useEffect(() => {
    getData();
  }, []);

  const handleChecked = (idTodos, isActive) => {
    axios.patch(`https://todo.api.devcode.gethired.id/todo-items/${idTodos}`, {
      is_active: isActive ? 0 : 1,
    });
  };

  const handleChangeActivity = () => {
    axios.patch(`https://todo.api.devcode.gethired.id/activity-groups/${id}`, {
      title: activity,
    });
    setInput(false);
  };

  const handleClickDelete = () => {
    axios
      .delete(`https://todo.api.devcode.gethired.id/todo-items/${todo.id}`)
      .then(() => {
        getData();
        toast({
          render: () => (
            <Box
              data-cy="modal-information"
              p={3}
              bg="white"
              shadow="md"
              borderWidth="1px"
              display="flex"
              alignItems="center"
              borderRadius={12}
            >
              <Image
                src={ImgAssets.Remove}
                mr={2}
                data-cy="modal-information-icon"
              />
              <Text data-cy="modal-information-title">
                Todo berhasil dihapus
              </Text>
            </Box>
          ),
          position: 'top-right',
          status: 'success',
          duration: 3000,
          isClosable: true,
        });

        setIsOpenDelete(false);
      })
      .catch(err => console.log(err));
  };

  const handleSort = priority => {
    setPriority(priority);
    let dataClone = [...data];
    switch (priority) {
      case 'terbaru':
        const sortedDesc = data.sort(
          (objA, objB) => Number(objB.id) - Number(objA.id)
        );
        setData(sortedDesc);
        break;
      case 'terlama':
        const sortedAsc = dataClone.sort(
          (objA, objB) => Number(objA.id) - Number(objB.id)
        );
        setData(sortedAsc);
        break;
      case 'a-z':
        const sortedAtoZ = dataClone.sort(function (a, b) {
          return a.title > b.title ? 1 : a.title === b.title ? 0 : -1;
        });
        setData(sortedAtoZ);
        break;
      case 'z-a':
        const sortedZtoA = dataClone.sort(function (a, b) {
          return b.title > a.title ? 1 : b.title === a.title ? 0 : -1;
        });
        setData(sortedZtoA);
        break;

      default:
        const sortBlmSelesai = dataClone.sort(function (a, b) {
          return b.is_active - a.is_active;
        });
        setData(sortBlmSelesai);
        break;
    }
  };

  return (
    <Layout>
      {/* secion of add */}
      <VStack pt={43} pb={38}>
        <Container maxW="1000px" color="white">
          <Grid templateColumns="repeat(2, 1fr)" gap={4}>
            <GridItem
              display="flex"
              justifyContent="flex-start"
              alignItems="center"
              h="10"
            >
              <Image
                data-cy="todo-back-button"
                src={ImgAssets.ArrowLeft}
                alt="arrow-left-icon"
                mr={5}
                width="10px"
                height="16px"
                cursor="pointer"
                onClick={() => navigate('/')}
              />
              {isInput ? (
                <Input
                  variant="flushed"
                  value={activity}
                  onChange={e => setActivity(e.target.value)}
                  color="#111111"
                  fontSize={36}
                  fontWeight="bold"
                  onBlur={handleChangeActivity}
                />
              ) : (
                <Text
                  data-cy="todo-title"
                  fontSize={36}
                  fontWeight="bold"
                  color="#111111"
                  onClick={() => setInput(!isInput)}
                >
                  {activity}
                </Text>
              )}
              <Image
                data-cy="todo-title-edit-button"
                src={ImgAssets.PencilIcon}
                alt="pencil-icon"
                ml={16}
                width="16px"
                height="16px"
                cursor="pointer"
                onClick={() => setInput(!isInput)}
              />
            </GridItem>
            <GridItem
              colStart={4}
              colEnd={6}
              display="flex"
              justifyContent="flex-end"
              h="10"
            >
              {/* start of filter */}
              <Menu>
                <MenuButton
                  data-cy="todo-sort-button"
                  as={Button}
                  border="1px solid black"
                  padding="10px"
                  borderRadius="50%"
                  variant="outline"
                >
                  <Image src={ImgAssets.Filter} width="18px" height="14px" />
                </MenuButton>
                <MenuList>
                  {dataOption.map(list => (
                    <MenuItem
                      data-cy="sort-selection"
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                      onClick={() => handleSort(list.priority)}
                    >
                      <Grid display="flex" alignItems="center">
                        <Image
                          data-cy="sort-selection-icon"
                          src={list.imgAsset}
                          mr={5}
                          alt={list.title}
                        />
                        <Text data-cy="sort-selection-title" color="#4A4A4A">
                          {list.title}
                        </Text>
                      </Grid>
                      {list.priority === priority && (
                        <CheckIcon color="#4A4A4A" />
                      )}
                    </MenuItem>
                  ))}
                </MenuList>
              </Menu>
              {/* end of filter */}
              <Button
                ml={5}
                leftIcon={<AddIcon />}
                colorScheme="teal"
                variant="solid"
                bg="#16ABF8"
                borderRadius={50}
                data-cy="todo-add-button"
                onClick={() => setIsOpen(true)}
              >
                Tambah
              </Button>
            </GridItem>
          </Grid>
        </Container>
      </VStack>
      <DetailContainer
        isLoading={isLoading}
        data={data}
        handleDelete={() => setIsOpenDelete(true)}
        handleEdit={() => setIsOpen(true)}
        handleChecked={handleChecked}
      />
      <ModalForm
        idActivity={id}
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        getData={getData}
      />
      <ModalConfirm
        isOpen={isOpenDelete}
        setIsOpen={setIsOpenDelete}
        desc="Apakah anda yakin menghapus List Item"
        title={todo.title}
        handleClickDelete={handleClickDelete}
      />
    </Layout>
  );
}

export default Detail;
