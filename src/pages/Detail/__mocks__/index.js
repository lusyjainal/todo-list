import ImgAssets from '../../../assets/images';

export const dataOption = [
  {
    imgAsset: ImgAssets.Terbaru,
    title: 'Terbaru',
    priority: 'terbaru',
  },
  {
    imgAsset: ImgAssets.Terlama,
    title: 'Terlama',
    priority: 'terlama',
  },
  {
    imgAsset: ImgAssets.Az,
    title: 'A-Z',
    priority: 'a-z',
  },
  {
    imgAsset: ImgAssets.Za,
    title: 'Z-A',
    priority: 'z-a',
  },
  {
    imgAsset: ImgAssets.BelumSelesai,
    title: 'Belum Selesai',
    priority: 'belum-selesai',
  },
];
