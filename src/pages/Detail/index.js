import { connect } from 'react-redux';
import Detail from './Detail';
import { bindActionCreators } from 'redux';

const mapStateToProps = state => {
  return {
    todo: state.todo
  };
}

export default connect(mapStateToProps, null)(Detail);
