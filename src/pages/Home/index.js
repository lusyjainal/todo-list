import React, { useState, useEffect } from 'react';
import { AddIcon } from '@chakra-ui/icons';
import {
  Button,
  Container,
  Grid,
  GridItem,
  Text,
  VStack,
  useToast,
  Box,
  Image,
} from '@chakra-ui/react';
import axios from 'axios';
import Layout from '../../components/Layout';
import { HomeContainer } from '../../containers';
import { ModalConfirm } from '../../components/section';
import ImgAssets from '../../assets/images';

function Home() {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [isOpenDelete, setIsOpenDelete] = useState(false);
  const [title, setTitle] = useState('');
  const [id, setId] = useState('');
  const toast = useToast();

  const getData = () => {
    setLoading(true);
    axios
      .get(
        `https://todo.api.devcode.gethired.id/activity-groups?email=lusyjainal@gmail.com`
      )
      .then(response => {
        const { data } = response.data;
        setData(data);
        setLoading(false);
      })
      .catch(err => console.log(err));
  };

  useEffect(() => {
    getData();
  }, []);

  const handleAdd = () => {
    axios
      .post(
        `https://todo.api.devcode.gethired.id/activity-groups?email=lusyjainal@gmail.com`,
        {
          title: `New Activity`,
          email: 'lusyjainal@gmail.com',
        }
      )
      .then(() => getData())
      .catch(err => console.log(err));
  };

  const handleDelete = (title, id) => {
    setIsOpenDelete(true);
    setTitle(title);
    setId(id);
  };

  const handleClickDelete = () => {
    axios
      .delete(`https://todo.api.devcode.gethired.id/activity-groups/${id}`)
      .then(() => {
        toast({
          render: () => (
            <Box
              data-cy="modal-information"
              p={3}
              bg="white"
              shadow="md"
              borderWidth="1px"
              display="flex"
              alignItems="center"
              borderRadius={12}
            >
              <Image src={ImgAssets.Remove} mr={2} data-cy="modal-information-icon"/>
              <Text data-cy="modal-information-title">Activity berhasil dihapus</Text>
            </Box>
          ),
          position: 'top-right',
          status: 'success',
          duration: 3000,
          isClosable: true,
        });
        getData();
        setIsOpenDelete(false);
      })
      .catch(err => console.log(err));
  };

  return (
    <Layout>
      {/* secion of add */}
      <VStack pt={43} pb={38}>
        <Container maxW="1000px" color="white">
          <Grid templateColumns="repeat(5, 1fr)" gap={4}>
            <GridItem colSpan={2} h="10">
              <Text
                fontSize={36}
                fontWeight="bold"
                color="#111111"
                data-cy="activity-title"
              >
                Activity
              </Text>
            </GridItem>
            <GridItem
              colStart={4}
              colEnd={6}
              display="flex"
              justifyContent="flex-end"
              h="10"
            >
              <Button
                leftIcon={<AddIcon />}
                colorScheme="teal"
                variant="solid"
                bg="#16ABF8"
                borderRadius={50}
                data-cy="activity-add-button"
                onClick={() => handleAdd()}
              >
                Tambah
              </Button>
            </GridItem>
          </Grid>
        </Container>
      </VStack>
      <HomeContainer
        data={data}
        isLoading={isLoading}
        handleDelete={handleDelete}
      />

      <ModalConfirm
        isOpen={isOpenDelete}
        setIsOpen={setIsOpenDelete}
        desc="Apakah anda yakin menghapus activity"
        title={title}
        handleClickDelete={handleClickDelete}
      />
    </Layout>
  );
}

export default Home;
