import { ACTIONS } from '../../constants';

export function getTodo(todo) {
  return {
    type: ACTIONS.GET_TODO,
    todo
  };
};