import { ACTIONS } from '../../constants';

const todo = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.GET_TODO:
      return action.todo;
    default:
      return state;
  }
}

export default todo;

